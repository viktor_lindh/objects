
public class Cykel extends Fordon {

	private String namn;
	private int trampor;
	
	
	public String getNamn() {
		return namn;
	}
	public void setNamn(String namn) {
		this.namn = namn;
	}	
		
	public int getTrampor() {
		return trampor;
	}
	public void setTrampor(int trampor) {
		this.trampor = trampor;
	
	}
	
	public void printinfo(){
		super.printDescription();
		
		
		System.out.println("Antal trampor: "+getTrampor()+"\n");
	}
	public String toString(){
	
	return "Modell: "+getNamn()+"\nAntal trampor: "+getTrampor()+"\n"+super.toString();
	
	}

}