
public class Kund {
	
	private String namn;
	private Produkt köptProdukt;
	
	
	public String getNamn() {
		return namn;
	}
	public void setNamn(String namn) {
		this.namn = namn;
	}
	public Produkt getKöptFordon() {
		return köptProdukt;
	}
	public void setköptProdukt(Produkt köptProdukt) {
		this.köptProdukt = köptProdukt;
	}
	
	public void printDescription(){
		System.out.println("------------------------\n"+"Kund: "+this.namn+"\n------------------------"+"\n"+köptProdukt+"\n");
		
	}


}
