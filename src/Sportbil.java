
public class Sportbil extends Fordon {
	private int hastighet;
	private String namn;
	
	public String getNamn() {
		return namn;
	}


	public void setNamn(String namn) {
		this.namn = namn;
	}


	public void setHastighet(int hastighet) {
		this.hastighet = hastighet;
	}


	public int getHastighet() {
		return hastighet;
	}


public void printDescription() {
	super.printDescription();
	
	System.out.println("Modell: "+this.namn+ "\n");
	System.out.println("Sportbilens max hastighet �r: " + this.hastighet + " KM/h \n");
	
	}
	public String toString(){
		
		return "Modell: "+this.namn+ "\n"+"Top fart: " + this.hastighet + " KM/h \n"+super.toString();
	}
}

